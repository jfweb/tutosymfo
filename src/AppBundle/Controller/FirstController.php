<?php
/**
 * Created by PhpStorm.
 * User: Jfweb
 * Date: 20/02/2018
 * Time: 11:46
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\FirstFormType;

class FirstController extends Controller
{
    /**
     * @Route("/first/{page}", name="firstAction", defaults={"page" = null})
     */
    public function firstAction(Request $request, $page)
    {
        // Pour les GET : $request->query
        // Pour les POST : $request->request

        $form = $this->createForm(FirstFormType::class);
        $form->handleRequest($request);

        if($form->isValid()){
            dump($request->request);
        }

        return $this->render('default/first.html.twig',
            ['page' => $page,
             'form'=> $form->createView()]);
    }
}