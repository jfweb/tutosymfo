<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pays
 *
 * @ORM\Table(name="pays")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PaysRepository")
 */
class Pays
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pays_nom", type="string", length=255)
     */
    private $paysNom;

    /**
     * @var string
     *
     * @ORM\Column(name="pays_monnaie", type="string", length=255)
     */
    private $paysMonnaie;

    /**
     * @var string
     *
     * @ORM\Column(name="pays_cio", type="string", length=255)
     */
    private $paysCio;

    /**
     * @var string
     *
     * @ORM\Column(name="pays_iso", type="string", length=255)
     */
    private $paysIso;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="ClientBundle\Entity\Entreprises", mappedBy="entreprisePays")
     */
    private $entreprises;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paysNom
     *
     * @param string $paysNom
     *
     * @return Pays
     */
    public function setPaysNom($paysNom)
    {
        $this->paysNom = $paysNom;

        return $this;
    }

    /**
     * Get paysNom
     *
     * @return string
     */
    public function getPaysNom()
    {
        return $this->paysNom;
    }

    /**
     * Set paysMonnaie
     *
     * @param string $paysMonnaie
     *
     * @return Pays
     */
    public function setPaysMonnaie($paysMonnaie)
    {
        $this->paysMonnaie = $paysMonnaie;

        return $this;
    }

    /**
     * Get paysMonnaie
     *
     * @return string
     */
    public function getPaysMonnaie()
    {
        return $this->paysMonnaie;
    }

    /**
     * Set paysCio
     *
     * @param string $paysCio
     *
     * @return Pays
     */
    public function setPaysCio($paysCio)
    {
        $this->paysCio = $paysCio;

        return $this;
    }

    /**
     * Get paysCio
     *
     * @return string
     */
    public function getPaysCio()
    {
        return $this->paysCio;
    }

    /**
     * Set paysIso
     *
     * @param string $paysIso
     *
     * @return Pays
     */
    public function setPaysIso($paysIso)
    {
        $this->paysIso = $paysIso;

        return $this;
    }

    /**
     * Get paysIso
     *
     * @return string
     */
    public function getPaysIso()
    {
        return $this->paysIso;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entreprises = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add entreprise
     *
     * @param \ClientBundle\Entity\Entreprises $entreprise
     *
     * @return Pays
     */
    public function addEntreprise(\ClientBundle\Entity\Entreprises $entreprise)
    {
        $this->entreprises[] = $entreprise;

        return $this;
    }

    /**
     * Remove entreprise
     *
     * @param \ClientBundle\Entity\Entreprises $entreprise
     */
    public function removeEntreprise(\ClientBundle\Entity\Entreprises $entreprise)
    {
        $this->entreprises->removeElement($entreprise);
    }

    /**
     * Get entreprises
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntreprises()
    {
        return $this->entreprises;
    }
}
