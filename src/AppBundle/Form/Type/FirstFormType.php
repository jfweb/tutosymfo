<?php
/**
 * Created by PhpStorm.
 * User: Jfweb
 * Date: 20/02/2018
 * Time: 14:14
 */

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class FirstFormType extends AbstractType
{
    public function BuildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('input_1', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('submit', SubmitType::class);
    }
}