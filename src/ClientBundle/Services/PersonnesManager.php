<?php
/**
 * Created by PhpStorm.
 * User: Jfweb
 * Date: 22/02/2018
 * Time: 16:31
 */

namespace ClientBundle\Services;

use Doctrine\ORM\EntityManager;
use ClientBundle\Entity\Personnes;
use ClientBundle\Entity\Entreprises;

class PersonnesManager
{
    private $em;
    private $container;
    private $repositoryPersonnes;
    private $repositoryEntreprises;

    function __construct(EntityManager $entityManager, $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryPersonnes = $this->em->getRepository('ClientBundle:Personnes');
        $this->repositoryEntreprises = $this->em->getRepository('ClientBundle:Entreprises');
    }

    public function infosPersonne($data, $personne = null)
    {
        $entreprise = $this->repositoryEntreprises->find($data['entreprise_nom']);
        if($personne == null) {
            $personne = new Personnes();
        }

        $personne->setPersonneNom($data['personne_nom']);
        $personne->setPersonnePrenom($data['personne_prenom']);
        $personne->setEntreprisePersonne($entreprise);

        $this->em->persist($personne);
        $this->em->persist($entreprise);
        $this->em->flush($personne);
        $this->em->flush($entreprise);
    }
}