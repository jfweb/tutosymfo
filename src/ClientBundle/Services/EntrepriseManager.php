<?php
/**
 * Created by PhpStorm.
 * User: Jfweb
 * Date: 21/02/2018
 * Time: 16:05
 */

namespace ClientBundle\Services;

use Doctrine\ORM\EntityManager;
use ClientBundle\Entity\Entreprises;

class EntrepriseManager
{
    private $em;
    private $container;
    private $repositoryEntreprise;
    private $repositoryPays;

    function __construct(EntityManager $entityManager, $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
        $this->repositoryEntreprise = $this->em->getRepository('ClientBundle:Entreprises');
        $this->repositoryPays = $this->em->getRepository('AppBundle:Pays');
    }

    public function infosEntreprise($data, $entreprise = null)
    {
        $pays = $this->repositoryPays->find($data['entreprise_pays']);
        if($entreprise == null) {
            $entreprise = new Entreprises();
        }

        $entreprise->setEntrepriseNom($data['entreprise_nom']);
        $entreprise->setEntrepriseAdresse($data['entreprise_adresse']);
        $entreprise->setEntrepriseCp($data['entreprise_cp']);
        $entreprise->setEntrepriseVille($data['entreprise_ville']);
        $entreprise->setEntreprisePays($pays);

        $this->em->persist($entreprise);
        $this->em->flush($entreprise);

    }
}