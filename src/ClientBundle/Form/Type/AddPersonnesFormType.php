<?php
/**
 * Created by PhpStorm.
 * User: Jfweb
 * Date: 22/02/2018
 * Time: 15:11
 */

namespace ClientBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use ClientBundle\Entity\Entreprises;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AddPersonnesFormType  extends AbstractType
{
    public function BuildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('personne_nom', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('personne_prenom', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('personneEntreprise', EntityType::class, array('class' => Entreprises::Class , 'choice_label' => 'entrepriseNom', 'attr' => array('class' => 'form-control')))
            ->add('submit', SubmitType::class, array('attr' => array('class' => 'btn btn-primary')));
    }
}