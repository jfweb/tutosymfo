<?php
/**
 * Created by PhpStorm.
 * User: Jfweb
 * Date: 20/02/2018
 * Time: 14:14
 */

namespace ClientBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Pays;

class AddEntreprisesFormType extends AbstractType
{
    public function BuildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('entreprise_nom', TextType::class, array('attr' => array('class' => 'form-control', 'label_attr' => 'Nom de l\'entreprise')))
            ->add('entreprise_adresse', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('entreprise_cp', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('entreprise_ville', TextType::class, array('attr' => array('class' => 'form-control')))
            ->add('entreprise_pays', EntityType::class, array('class' => Pays::Class , 'choice_label' => 'paysNom', 'attr' => array('class' => 'form-control')))
            ->add('submit', SubmitType::class, array('attr' => array('class' => 'btn btn-primary')));
    }
}