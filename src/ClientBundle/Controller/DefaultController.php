<?php

namespace ClientBundle\Controller;

use ClientBundle\Entity\Entreprises;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\Type\AddEntreprisesFormType;

class DefaultController extends Controller
{
    /**
     * @Route("/liste-entreprise", name="liste-entreprise")
     */
    public function ListeEntrepriseAction()
    {
        $form = $this->createForm(AddEntreprisestFormType::class);
        $form->handleRequest($request);

        if($form->isValid()){
            dump($request->request);
        }

        return $this->render('index.html.twig',
            ['page' => $page,
                'form'=> $form->createView()]);

//        return $this->render('ClientBundle:Default:index.html.twig');
    }
}
