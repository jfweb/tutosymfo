<?php
/**
 * Created by PhpStorm.
 * User: Jfweb
 * Date: 21/02/2018
 * Time: 10:14
 */

namespace ClientBundle\Controller;

use ClientBundle\Entity\Entreprises;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use ClientBundle\Form\Type\AddEntreprisesFormType;

class EntreprisesController extends Controller
{
    /**
     * @Route("/liste-entreprise", name="liste-entreprise")
     */
    public function listeEntrepriseAction(Request $request)
    {
        $form = $this->createForm(AddEntreprisesFormType::class);
        $form->handleRequest($request);

        if($form->isValid())
        {
            $entrepriseManager = $this->container->get('client.entreprisemanager');
            $entrepriseManager->infosEntreprise($form->getData());
            $form = $this->createForm(AddEntreprisesFormType::class);
        }

        $repository = $this->getDoctrine()->getRepository('ClientBundle:Entreprises');
        $listeEntreprise = $repository->findAll();

        return $this->render('default/liste-entreprise.html.twig',
            [
                'listeEntreprises' => $listeEntreprise,
                'form'=> $form->createView()
            ]);
    }

    /**
     * @Route("/fiche-entreprise/{id}", name="fiche-entreprise", defaults={"id" = null})
     */
    public function ficheEntrepriseAction(Request $request, $id)
    {
        if($id != null)
        {
            $form = $this->createForm(AddEntreprisesFormType::class);
            $form->handleRequest($request);

            $repository = $this->getDoctrine()->getRepository('ClientBundle:Entreprises');
            $entreprise = $repository->find($id);

            if($form->isValid())
            {
                $entrepriseManager = $this->container->get('client.entreprisemanager');

                $entrepriseManager->infosEntreprise($form->getData(), $entreprise);
            }

            return $this->render('default/fiche-entreprise.html.twig',
                [
                    'ficheEntreprise' => $entreprise,
                    'form'=> $form->createView(),
                ]);
        }
        else {
            return $this->redirectToRoute('liste-entreprise');
        }
    }

    /**
     * @Route("/delete-entreprise/{id}", name="delete-entreprise", defaults={"id" = null})
     */
    public function removeEntrepriseAction(Request $request, $id)
    {
        if($id != null)
        {
            $repository = $this->getDoctrine()->getRepository('ClientBundle:Entreprises');
            $deleteEntreprise = $repository->find($id);

            if($deleteEntreprise)
            {
                $em = $this->getDoctrine()->getEntityManager();
                $em->remove($deleteEntreprise);
                $em->flush();
            }
            return $this->redirectToRoute('liste-entreprise');
        }
    }

    /**
     * @Route("/entrepriseByPays/{id}", name="entrepriseByPays")
     */
    public function entrepriseByPaysAction($id)
    {
        $entrepriseByPays =
                $this->getDoctrine()
                    ->getRepository('ClientBundle:Entreprises')
                    ->entrepriseByPays($id);

        dump($entrepriseByPays);

        die();
    }
}