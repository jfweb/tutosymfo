<?php
/**
 * Created by PhpStorm.
 * User: Jfweb
 * Date: 22/02/2018
 * Time: 14:36
 */

namespace ClientBundle\Controller;

use ClientBundle\Entity\Personnes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use ClientBundle\Form\Type\AddPersonnesFormType;

class PersonnesController extends Controller
{
    /**
     * @Route("/liste-personne", name="liste-personne")
     */
    public function listePersonnesAction(Request $request)
    {
        $form = $this->createForm(AddPersonnesFormType::class);
        $form->handleRequest($request);

        if($form->isValid())
        {
            $personneManager = $this->container->get('client.personnemanager');
            $personneManager->infosPersonne($form->getData());
            $form = $this->createForm(AddPersonnesFormType::class);
        }

        $repository = $this->getDoctrine()->getRepository('ClientBundle:Personnes');
        $listePersonne = $repository->findAll();

        return $this->render('default/liste-personne.html.twig',
            [
                'form'=> $form->createView(),
                'listePersonne' => $listePersonne
            ]);
    }


    /**
     * @Route("/fiche-personne/{id}", name="fiche-personne", defaults={"id" = null})
     **/
    public function fichePersonneAction(Request $request, $id)
    {
        if($id != null)
        {
            $form = $this->createForm(AddPersonnesFormType::class);
            $form->handleRequest($request);

            $repository = $this->getDoctrine()->getRepository('ClientBundle:Personnes');
            $personne = $repository->find($id);

            if($form->isValid())
            {
                $personneManager = $this->container->get('client.personnemanager');

                $personneManager->infosPersonne($form->getData(), $personne);
            }

            return $this->render('default/fiche-personne.html.twig',
                [
                    'fichePersonne' => $personne,
                    'form'=> $form->createView(),
                ]);
        }
        else {
            return $this->redirectToRoute('liste-personne');
        }
    }

    /**
     * @Route("/delete-personne/{id}", name="delete-personne", defaults={"id" = null})
     */
    public function removePersonneAction(Request $request, $id)
    {
        if($id != null)
        {
            $repository = $this->getDoctrine()->getRepository('ClientBundle:Personnes');
            $deletePersonne = $repository->find($id);

            if($deletePersonne)
            {
                $em = $this->getDoctrine()->getEntityManager();
                $em->remove($deletePersonne);
                $em->flush();
            }
            return $this->redirectToRoute('liste-personne');
        }
    }

}