<?php

namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personnes
 *
 * @ORM\Table(name="personnes")
 * @ORM\Entity(repositoryClass="ClientBundle\Repository\PersonnesRepository")
 */
class Personnes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="personne_nom", type="string", length=255)
     */
    private $personneNom;

    /**
     * @var string
     *
     * @ORM\Column(name="personne_prenom", type="string", length=255)
     */
    private $personnePrenom;

    /***
     * @ORM\ManyToOne(targetEntity="ClientBundle\Entity\Entreprises", inversedBy="entreprisePersonne")
     */
    private $entreprise;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entreprise = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add entreprise
     *
     * @param \ClientBundle\Entity\Entreprises $entreprise
     *
     * @return Personnes
     */
    public function addEntreprise(\ClientBundle\Entity\Entreprises $entreprise)
    {
        $this->entreprise[] = $entreprise;

        return $this;
    }

    /**
     * Remove entreprise
     *
     * @param \ClientBundle\Entity\Entreprises $entreprise
     */
    public function removeEntreprise(\ClientBundle\Entity\Entreprises $entreprise)
    {
        $this->entreprise->removeElement($entreprise);
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set personneNom
     *
     * @param string $personneNom
     *
     * @return Personnes
     */
    public function setPersonneNom($personneNom)
    {
        $this->personneNom = $personneNom;

        return $this;
    }

    /**
     * Get personneNom
     *
     * @return string
     */
    public function getPersonneNom()
    {
        return $this->personneNom;
    }

    /**
     * Set personnePrenom
     *
     * @param string $personnePrenom
     *
     * @return Personnes
     */
    public function setPersonnePrenom($personnePrenom)
    {
        $this->personnePrenom = $personnePrenom;

        return $this;
    }

    /**
     * Get personnePrenom
     *
     * @return string
     */
    public function getPersonnePrenom()
    {
        return $this->personnePrenom;
    }

    /**
     * Set personneEntreprise
     *
     * @param \AppBundle\Entity\Personnes $personneEntreprise
     *
     * @return Entreprises
     */
    public function setPersonneEntreprise(\AppBundle\Entity\Pays $personneEntreprise = null)
    {
        $this->personneEntreprise = $personneEntreprise;

        return $this;
    }

    /**
     * Get entreprises
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

}
