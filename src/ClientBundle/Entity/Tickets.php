<?php

namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tickets
 *
 * @ORM\Table(name="tickets")
 * @ORM\Entity(repositoryClass="ClientBundle\Repository\TicketsRepository")
 */
class Tickets
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ticket_titre", type="string", length=255)
     */
    private $ticketTitre;

    /**
     * @var int
     *
     * @ORM\Column(name="ticket_priorite", type="integer")
     */
    private $ticketPriorite;

    /**
     * @var string
     *
     * @ORM\Column(name="ticket_contenu", type="text")
     */
    private $ticketContenu;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ticket_ouverture", type="datetimetz")
     */
    private $ticketOuverture;

    /***
     * @ORM\ManyToOne(targetEntity="ClientBundle\Entity\Projets", inversedBy="tickets")
     */
    private $projets;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ticketTitre
     *
     * @param string $ticketTitre
     *
     * @return Tickets
     */
    public function setTicketTitre($ticketTitre)
    {
        $this->ticketTitre = $ticketTitre;

        return $this;
    }

    /**
     * Get ticketTitre
     *
     * @return string
     */
    public function getTicketTitre()
    {
        return $this->ticketTitre;
    }

    /**
     * Set ticketPriorite
     *
     * @param integer $ticketPriorite
     *
     * @return Tickets
     */
    public function setTicketPriorite($ticketPriorite)
    {
        $this->ticketPriorite = $ticketPriorite;

        return $this;
    }

    /**
     * Get ticketPriorite
     *
     * @return int
     */
    public function getTicketPriorite()
    {
        return $this->ticketPriorite;
    }

    /**
     * Set ticketContenu
     *
     * @param string $ticketContenu
     *
     * @return Tickets
     */
    public function setTicketContenu($ticketContenu)
    {
        $this->ticketContenu = $ticketContenu;

        return $this;
    }

    /**
     * Get ticketContenu
     *
     * @return string
     */
    public function getTicketContenu()
    {
        return $this->ticketContenu;
    }

    /**
     * Set ticketOuverture
     *
     * @param \DateTime $ticketOuverture
     *
     * @return Tickets
     */
    public function setTicketOuverture($ticketOuverture)
    {
        $this->ticketOuverture = $ticketOuverture;

        return $this;
    }

    /**
     * Get ticketOuverture
     *
     * @return \DateTime
     */
    public function getTicketOuverture()
    {
        return $this->ticketOuverture;
    }
}
