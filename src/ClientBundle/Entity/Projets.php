<?php

namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Projets
 *
 * @ORM\Table(name="projets")
 * @ORM\Entity(repositoryClass="ClientBundle\Repository\ProjetsRepository")
 */
class Projets
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="projets_nom", type="string", length=255)
     */
    private $projetsNom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="datetimetz")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="datetimetz")
     */
    private $dateFin;

    /***
     * @ORM\ManyToOne(targetEntity="ClientBundle\Entity\Entreprises", inversedBy="projets")
     */
    private $entreprises;

    /***
     * @ORM\OneToMany(targetEntity="ClientBundle\Entity\Tickets", inversedBy="projets")
     */
    private $tickets;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projetsNom
     *
     * @param string $projetsNom
     *
     * @return Projets
     */
    public function setProjetsNom($projetsNom)
    {
        $this->projetsNom = $projetsNom;

        return $this;
    }

    /**
     * Get projetsNom
     *
     * @return string
     */
    public function getProjetsNom()
    {
        return $this->projetsNom;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return Projets
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Projets
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }
}
