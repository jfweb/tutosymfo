<?php

namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personnes_postes
 *
 * @ORM\Table(name="personnes_postes")
 * @ORM\Entity(repositoryClass="ClientBundle\Repository\Personnes_postesRepository")
 */
class Personnes_postes
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="label_postes", type="string", length=255)
     */
    private $labelPostes;

    /***
     * @ORM\OneToOne(targetEntity="ClientBundle\Entity\Personnes", mappedBy="postes")
     */
    private $personnes;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set labelPostes
     *
     * @param string $labelPostes
     *
     * @return Personnes_postes
     */
    public function setLabelPostes($labelPostes)
    {
        $this->labelPostes = $labelPostes;

        return $this;
    }

    /**
     * Get labelPostes
     *
     * @return string
     */
    public function getLabelPostes()
    {
        return $this->labelPostes;
    }
}
