<?php

namespace ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entreprises
 *
 * @ORM\Table(name="entreprises")
 * @ORM\Entity(repositoryClass="ClientBundle\Repository\EntreprisesRepository")
 */
class Entreprises
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="entreprise_nom", type="string", length=255)
     */
    private $entrepriseNom;

    /**
     * @var string
     *
     * @ORM\Column(name="entreprise_adresse", type="text")
     */
    private $entrepriseAdresse;

    /**
     * @var string
     *
     * @ORM\Column(name="entreprise_cp", type="string", length=255)
     */
    private $entrepriseCp;

    /**
     * @var string
     *
     * @ORM\Column(name="entreprise_ville", type="string", length=255)
     */
    private $entrepriseVille;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Pays", inversedBy="entreprises")
     */
    private $entreprisePays;

    /***
     * @ORM\OneToMany(targetEntity="ClientBundle\Entity\Personnes", inversedBy="entreprise")
     */
    private $entreprisePersonne;

    /***
     * @ORM\OneToMany(targetEntity="ClientBundle\Entity\Projets", inversedBy="entreprises")
     */
    private $entreprises;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entrepriseNom
     *
     * @param string $entrepriseNom
     *
     * @return Entreprises
     */
    public function setEntrepriseNom($entrepriseNom)
    {
        $this->entrepriseNom = $entrepriseNom;

        return $this;
    }

    /**
     * Get entrepriseNom
     *
     * @return string
     */
    public function getEntrepriseNom()
    {
        return $this->entrepriseNom;
    }

    /**
     * Set entrepriseAdresse
     *
     * @param string $entrepriseAdresse
     *
     * @return Entreprises
     */
    public function setEntrepriseAdresse($entrepriseAdresse)
    {
        $this->entrepriseAdresse = $entrepriseAdresse;

        return $this;
    }

    /**
     * Get entrepriseAdresse
     *
     * @return string
     */
    public function getEntrepriseAdresse()
    {
        return $this->entrepriseAdresse;
    }

    /**
     * Set entrepriseCp
     *
     * @param string $entrepriseCp
     *
     * @return Entreprises
     */
    public function setEntrepriseCp($entrepriseCp)
    {
        $this->entrepriseCp = $entrepriseCp;

        return $this;
    }

    /**
     * Get entrepriseCp
     *
     * @return string
     */
    public function getEntrepriseCp()
    {
        return $this->entrepriseCp;
    }

    /**
     * Set entrepriseVille
     *
     * @param string $entrepriseVille
     *
     * @return Entreprises
     */
    public function setEntrepriseVille($entrepriseVille)
    {
        $this->entrepriseVille = $entrepriseVille;

        return $this;
    }

    /**
     * Get entrepriseVille
     *
     * @return string
     */
    public function getEntrepriseVille()
    {
        return $this->entrepriseVille;
    }

    /**
     * Set entreprisePays
     *
     * @param \AppBundle\Entity\Pays $entreprisePays
     *
     * @return Entreprises
     */
    public function setEntreprisePays(\AppBundle\Entity\Pays $entreprisePays = null)
    {
        $this->entreprisePays = $entreprisePays;

        return $this;
    }

    /**
     * Get entreprisePays
     *
     * @return \AppBundle\Entity\Pays
     */
    public function getEntreprisePays()
    {
        return $this->entreprisePays;
    }

    /**
     * Set entreprisePersonne
     *
     * @param \AppBundle\Entity\Personnes $entreprisePersonne
     *
     * @return Entreprises
     */
    public function setEntreprisePersonne(\AppBundle\Entity\Personnes $entreprisePersonne = null)
    {
        $this->entreprisePersonne =  $entreprisePersonne;

        return $this;
    }

    /**
     * Get entreprisePersonne
     *
     * @return \AppBundle\Entity\Entreprises
     */
    public function getEntreprisePersonne()
    {
        return $this->entreprisePersonne;
    }

}
